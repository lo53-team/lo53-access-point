#include "pcap-thread.h"
#include "platform.h"
#include "rssi_list.h"
#include "radiotap_iter.h"
#include <pcap.h>
#include <semaphore.h>
#include <signal.h>

extern volatile sig_atomic_t got_sigint;
extern Element * rssi_list;
extern sem_t synchro;

static const struct radiotap_align_size align_size_000000_00[] = {
  [0] = { .align = 1, .size = 4, },
  [52] = { .align = 1, .size = 4, },
};

static const struct ieee80211_radiotap_namespace vns_array[] = {
  {
    .oui = 0x000000,
    .subns = 0,
    .n_bits = sizeof(align_size_000000_00),
    .align_size = align_size_000000_00,
  },
};

static const struct ieee80211_radiotap_vendor_namespaces vns = {
  .ns = vns_array,
  .n_ns = sizeof(vns_array)/sizeof(vns_array[0]),
};


void *pcap_function(void *arg)
{
  char *iface = (char *) arg;
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t * handle = NULL;
  struct ieee80211_radiotap_header * rtap_head;
  struct ieee80211_header * eh;
  struct pcap_pkthdr header;
  const u_char * packet;
  u_char * mac;
  //u_char first_flags;
  //u_char second_flags;
  /*int offset = 0;
  int check = 1 ;*/
  //int start = 0;
  char rssi = 0;
  Element * dev_info;

  struct ieee80211_radiotap_iterator iter;
  int err;
  // Open pcap handle to sniff traffic
  handle = pcap_open_live(iface, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    printf("Could not open pcap on %s\n", iface);
    pthread_exit((void *) -1);
  }
  printf("open pcap on %s\n", iface);

  while (got_sigint == 0) {
    packet = pcap_next(handle, &header);
    if (!packet)
      continue;

    rtap_head = (struct ieee80211_radiotap_header *) packet;
    //calcul de la longeur du header de radiotap
    int16_t len = le16_to_cpu(rtap_head->it_len);

    eh = (struct ieee80211_header *) (packet + len);
    if (((eh->frame_control & 0x03) == 0x01)||((eh->frame_control & 0x03) == 0x00)) {
      mac = eh->source_addr;
      
      //creation de l'itérateur sur le header radiotap

      err = ieee80211_radiotap_iterator_init(&iter, (struct ieee80211_radiotap_header *)packet, 4096, &vns);
      if (err) {
        printf("malformed radiotap header (init returns %d)\n", err);
      }
    
     // check du flag pour verifier qu'on est vraiment à l'endroit ou lire le rssi
      while (!(err = ieee80211_radiotap_iterator_next(&iter))) {
        //start++;
        if (iter.this_arg_index == IEEE80211_RADIOTAP_DBM_ANTSIGNAL) {
            rssi = *iter.this_arg;
        }
      }
     
     
      sem_wait(&synchro);
      if ((dev_info = find_mac(rssi_list, mac)) == NULL) {
         
          dev_info = add_element(&rssi_list, mac);
      }
       printf("%d bytes -- %02X:%02X:%02X:%02X:%02X:%02X -- RSSI: %d dBm\n",len, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], (int) rssi);
      clear_outdated_values(&dev_info->measurements);
      add_value(&dev_info->measurements, (int) rssi);
      sem_post(&synchro);
    }
  }
  pcap_close(handle);
  printf("dehors capture\n");
  pthread_exit((void *) 0);
}
