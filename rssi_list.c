#include "rssi_list.h"


u_char *string_to_mac(char * buf, u_char * byte_mac)
{
	//printf(" start function string to mac \n ");
	int temp[6], i;

	int length = sscanf(buf,"%02x:%02x:%02x:%02x:%02x:%02x",&temp[0],&temp[1],&temp[2],&temp[3],&temp[4],&temp[5]);

	for(i=0; i<6; i++)
	{

		( byte_mac[i] )= (u_char)temp[i];

	}

	if(length!=6){

		printf("something went wrong");

	}


	//printf(" end of function string to mac \n");

	return byte_mac;
}

char * mac_to_string(u_char * byte_mac, char * buf){

	//printf("start function mac to string \n");

	int length=sprintf(buf,"%02x:%02x:%02x:%02x:%02x:%02x",byte_mac[0],byte_mac[1],byte_mac[2],byte_mac[3],byte_mac[4],byte_mac[5]);

	if(length!=17){

		printf("something went wrong");

	}

	//printf("end of function mac to string \n");

	return buf;

}
void clear_outdated_values(Deque * list){

	Rssi_sample * current=NULL;
	Rssi_sample * previous=NULL;
	unsigned long long now = get_timestamps_in_microseconds();

	if(list->head==NULL){
		printf("Attention!! liste de Rssi_sample est vide\n" );
		return;
	}else{

		current=list->head;

		while(current!=NULL){
			if(current->deadline<=now){
				if(previous==NULL){
					//printf("valeur de previous non null");
					list->head=current->next;
					free(current);
					current=list->head;

				}else{
					//printf("previous existe\n" );

					previous->next=current->next;
					free(current);
					current=previous->next;
				}

			}else{
				//printf(" la valeur n est pas outdated \n");
				previous=current;
				current=current->next;
			}

			

		}

	}

	
}

unsigned long long get_timestamps_in_microseconds(){
	time_t t;
	time(&t); 

	return (unsigned long long)1000000*t;
}

void clear_values(Deque * list){

	Rssi_sample * tmp = list->head;

	while(tmp!=NULL){

		list->head=tmp->next;

		free(tmp);

		tmp=list->head;
	}
}

void add_value(Deque * list, int value){

	Rssi_sample * nouvelElement=malloc(sizeof(Rssi_sample));
	//double calc=(value/10);
	//pow(((double)10),calc);
	nouvelElement->rssi_mW = value;
	nouvelElement->deadline = get_timestamps_in_microseconds()+DEFAULT_KEEP_DELAY;
	nouvelElement->next = NULL;

	if(list->head==NULL){

		list->head=nouvelElement;
		list->tail = nouvelElement;
	}else{

		list->tail->next=nouvelElement;
		list->tail=list->tail->next;
	}

}

void clear_list(Element ** list){
	Element * current = (*list);

	while(current!=NULL){

		*list=current->next;
		clear_values(&(current->measurements));
		free(current);
		current=*list;
	}
	list=NULL;
}

Element * find_mac(Element * list, u_char * mac_value){
	//printf("debut de la fonction find mac\n");
	Element * current = list;
	char * buf1 = malloc(17*sizeof(char));
	char * buf2 = malloc(17*sizeof(char));
	char * buf3 = malloc(17*sizeof(char));
	int len = strlen(mac_to_string(current->mac_addr, buf3));
	//printf("valeur du length %d \n", len );
	
	
	//printf(" adresse mac à chercher! %s",mac_to_string(mac_value, buf3));
	if (current == NULL)
	{
		printf("No Devices !!!\n");
		free(buf1);
		free(buf2);
		free(buf3);
		return NULL;
	}
	while(current != NULL){
	//	printf(" valeur du current adresse mac ! %s \n",mac_to_string(current->mac_addr, buf3));
		if (( strncmp(mac_to_string(current->mac_addr, buf1), mac_to_string(mac_value, buf2), len) == 0 ))
		{	//printf(" adresse mac trouvée!! %s\n",buf1);
			free(buf1);
			free(buf2);
			free(buf3);
			//printf("fin de la fonction find mac\n");
			return current;
		}
		current = current->next;
	}
	printf("Devices not found. \n");
	free(buf1);
	free(buf2);
	free(buf3);
	return NULL;
}


Element * add_element(Element ** list, u_char * mac_value){
	Element * current = *list;
	Element * nouvelElement = malloc(sizeof(Element));
	int i;

	for(i=0;i<6;i++) {

		nouvelElement->mac_addr[i] = mac_value[i];
	}
	nouvelElement->measurements.head = NULL;
	nouvelElement->measurements.tail = NULL;
	nouvelElement->next = NULL;

	if(current==NULL){
		current=nouvelElement;
		return *list;
	}else{
		while(current->next != NULL){
			current = current->next;
		}
		current->next = nouvelElement;
		return *list;
	}
}

void delete_element(Element ** list, Element * e){
	Element  *current = *list;
	Element * previous = NULL;
	int len=17;
	char * buf = malloc(17*sizeof(char));
	char * buf1 = malloc(17*sizeof(char));
	char * buf2 = malloc(17*sizeof(char));
		//printf(" l'adresse à supprimer %s\n", mac_to_string(e->mac_addr,buf) );
	if(current==NULL){
		printf("Attention!! liste d'éléments est vide\n" );
		return;
	}else{

			if(find_mac(current, e->mac_addr)!= NULL)
			{
				printf("l'élement existe\n");
		while(current!=NULL){
			printf("l'adresse mac du current %s\n", mac_to_string(current->mac_addr, buf1));
			if(( strncmp(mac_to_string(current->mac_addr, buf1), mac_to_string(e->mac_addr, buf2), len) == 0 )){
				printf("element trouvé suppression\n");
				
				printf(" l'adresse à supprimer %s\n", mac_to_string(e->mac_addr,buf) );
				if(previous==NULL){
					printf("valeur de previous non null\n");
					printf(" element trouvé à l interieur suppression!");
					*list=(current)->next;
					free(current);
					(current)=*list;

				}else{
					printf("previous existe\n" );

					previous->next=current->next;
					free(current);
					current=previous->next;
					
				}

			}else{
			
				previous=current;
				current=current->next;
			}

			

		}
	}
				free(buf1);
				free(buf2);
				free(buf);
	}
	
}

void clear_empty_macs(Element ** list){
	Element * current = *list;
	Element * previous = NULL;
	if (current == NULL)
	{
		printf("No device found. \n");
	}else{
		
		//First element
		if (current->next == NULL)
		{
			if (current->measurements.head == NULL)
			{
				free(current);
				*list = NULL;
				printf("Element correctly deleted. Your list is now empty.\n");
			}
		}else{
			//More than one element in the list
			previous=current;
			current = current->next;
			while(current != NULL){
				if (current->measurements.head == NULL )
				{
					previous->next = current->next;
					free(current);
					current = previous->next;
					printf("Element correctly deleted.\n");
				}else{
					previous = current;
					current = current->next;
				}
			}
		}
		
	}
}
char * build_element(Element * e, char * buf){

	Rssi_sample * current = e->measurements.head;
	char * tmp=malloc(7*sizeof(char));
	char *temp;
	char *tmpjson="{\"values\":[";
	char *tmpfin="]}";
		int i;

		for(i=0;i<11;i++){

			buf[i]=tmpjson[i];
		}


temp=&buf[i];
		

while(current!=NULL){
sprintf(tmp,"%.2f",current->rssi_mW);


strcat(temp,tmp);
if(current->next!=NULL){
strcat(temp,",");
}

current = current->next;

}

strcat(temp,tmpfin);

printf("%s\n",buf );
return buf;
	
}