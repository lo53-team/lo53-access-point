#include <stdio.h>
#include <stdlib.h>
#include "rssi_list.h"
#include <pthread.h>
#include "pcap-thread.h"
#include <signal.h>
#include <semaphore.h>
#include "hellobrowser.h"
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <microhttpd.h>
#include <string.h>



void afficheliste(Deque *deque){
		Rssi_sample *current = deque->head;
	while( current!=NULL){
		printf(" \n la mesure de rssi  : %.2f \n",current->rssi_mW );

		current=current->next;

	}
}

void afficheelement(Element ** list){
	printf("début de la fonction d'affichage d'élement\n");
	Element *current = *list;
	while( current!=NULL){
		printf("affichage d élément \n");
		afficheliste(&current->measurements);
			char *buffer=malloc(17*sizeof(char));

			 u_char * bytemac = malloc(6*sizeof(u_char));
			bytemac=current->mac_addr;
			printf(" %s \n",mac_to_string(bytemac,buffer));
		current=current->next;

	}
}

volatile sig_atomic_t got_sigint=0;
Element * rssi_list;

sem_t synchro;
char* page;

// implémentation de la réponse à une requete http envoyée
int answer_to_connection (void *cls, struct MHD_Connection *connection, 
                          const char *url, 
                          const char *method, const char *version, 
                          const char *upload_data, 
                          size_t *upload_data_size, void **con_cls)
{ 
     memset(page, 0x0, 1024);
	printf("l'url demandée : %s\n", url);
       Element* tmpElement = malloc(sizeof(Element));
     //char* buf4 = "64:CC:2E:6F:40:61";
    char* buf5= malloc(17*sizeof(char));
    int i=0;
    for (i = 0; i < 17; i++)
        {
        	buf5[i]=url[i+1];
        }  
          printf(" l'adresse mac à trouver :%s\n", buf5);
      u_char * bytemac1 = malloc(6*sizeof(u_char));
     sem_wait(&synchro);

     tmpElement = find_mac(rssi_list,string_to_mac(buf5,bytemac1));

    //build_element(tmpElement,buf5);

   // printf(" le buffer avant envoie : %s\n", build_element(tmpElement,buf5));
  //const char *page  = "<html><body>Hello, browser!</body></html>";

     if(tmpElement!=NULL){
    build_element(tmpElement,page);
 }

    
    printf("page rempli\n");
  struct MHD_Response *response;
  int ret;

  response = MHD_create_response_from_buffer (strlen(page),
                                            (void*)page, MHD_RESPMEM_PERSISTENT);

  ret = MHD_queue_response (connection, MHD_HTTP_OK, response);

  MHD_destroy_response (response);
   //free(listElement1);
    free(buf5);
    free(bytemac1);
 printf("fin de la fonction http\n");
 sem_post(&synchro);
  return ret;
}


int main(int argc, char* argv[])
{
    sem_init(&synchro,0,1);
    rssi_list= malloc(sizeof(Element));
    page = malloc(1024 * sizeof(char));
    printf("start\n");


    /****************************************** CODE ECRIT POUR LES TESTS DES FONCTIONS******************************************************************************
   // char *buffer = "00:0a:3f:cd:02:5f";
    //char *buffertest = "05:0c:3f:cd:02:5f";
    //u_char * bytemac = malloc(6*sizeof(u_char));

	//char *buffer1=malloc(17*sizeof(char));

	//int j=0;


  //  bytemac=string_to_mac(buffer,bytemac);
	
	// if((bytemac==NULL)||(buffer==NULL))
	// {
	// 	printf("ca ne marche pas");
	// }
	// for(j=0;j<6;j++){
	// printf("%02x",bytemac[j]);
	// }
	//buffer1=mac_to_string(bytemac,buffer1);


	// printf(" la chaine reconvertie est : %s\n",buffer1);

 //    printf("Hello world!\n");

 //    printf(" Début de test des fonctions\n");

   // Rssi_sample *R1=malloc(sizeof(Rssi_sample)), *R2=malloc(sizeof(Rssi_sample)), *R3=malloc(sizeof(Rssi_sample));

   // Element *E1=malloc(sizeof(Element)), *E2=malloc(sizeof(Element)), *E3=malloc(sizeof(Element)), **listElement=malloc(sizeof(Element*)),*Edel=malloc(sizeof(Element));

    //Element **listElement=malloc(sizeof(Element*));
    // R3->rssi_mW=50;
    // R3->deadline=get_timestamps_in_microseconds()+DEFAULT_KEEP_DELAY;
    // R3->next=NULL;
    // R2->rssi_mW=70;
    // R2->deadline=get_timestamps_in_microseconds()+DEFAULT_KEEP_DELAY;
    // R2->next=R3;
    // R1->rssi_mW=80;
    // R1->deadline=get_timestamps_in_microseconds()+DEFAULT_KEEP_DELAY;
    // R1->next=R2;

    // Deque *rssi_device=malloc(sizeof(Deque));
    // rssi_device->head=R1;
    // rssi_device->tail=R3;

   //  E3->measurements=*rssi_device;
   //  int k;
   //    u_char * byte = malloc(6*sizeof(u_char));
   // byte =string_to_mac(buffertest,bytemac);
   //  for (k = 0; k < 6; k++)
   //  {
   //  	E2->mac_addr[k]=byte[k];
   //  	Edel->mac_addr[k]=bytemac[k];
   //  	/* code */
   //  }
   //  E3->next=NULL;
   //  E2->measurements=*rssi_device;
   //  E2->next=E3;
   //  E1->measurements=*rssi_device;
   //  E1->next=E2;
    //*listElement=E1;

//afficheliste(rssi_device);
//sleep(2);
//clear_outdated_values(rssi_device);
//clear_values(rssi_device);
// add_value(rssi_device,-35);
// //afficheliste(rssi_device);
// //clear_list(listElement);
// add_element(listElement,string_to_mac(buffer,bytemac));
// add_element(listElement,string_to_mac(buffer,bytemac));
//printf(" l'adresse mac ajoutée %s\n", mac_to_string(string_to_mac(buffer,bytemac),buffer) );
    // for (k = 0; k < 6; k++)
    // {
    //     Edel->mac_addr[k]=bytemac[k];
    //     /* code */
    // }
  //E3=find_mac(*listElement,bytemac);

    //delete_element(listElement,Edel);

//clear_empty_macs(listElement);
//afficheelement(listElement);
  //bytemac=E3->mac_addr;
//printf("l'adresse mac trouvé est %s\n", mac_to_string(bytemac,buffer1) );
    //build_element(E3,buffer1);

/************************************************FIN DES TESTS DE FONCTIONS*****************************************************************************/


    //initialisation du serveur http et lancement en arrière plan

  struct MHD_Daemon *daemon;

  daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY, PORT, NULL, NULL, 
                             &answer_to_connection, NULL, MHD_OPTION_END);
  if (NULL == daemon) return 1;

  //lancement du thread qui s'occupe de lire les paquets

    pthread_t pcap_thread;
  pthread_create(&pcap_thread, NULL, pcap_function,argv[1]);


  pthread_join(pcap_thread,NULL);

  sem_destroy(&synchro);
  free(rssi_list);
    MHD_stop_daemon (daemon);
 
    return 0;
}
