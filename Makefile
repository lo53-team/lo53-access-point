PREFIX=/home/lo53/trunk/staging_dir
export STAGING_DIR=/home/lo53/trunk/staging_dir
GCC=$(PREFIX)/toolchain/bin/mips-openwrt-linux-gcc
LD=$(PREFIX)/toolchain/bin/mips-openwrt-linux-ld
ALLFLAGS=-Wall -Werror -O2 -I$(PREFIX)/toolchain/include/ -I$(PREFIX)/target/usr/include/ -L$(PREFIX)/toolchain/lib/ -L$(PREFIX)/target/usr/lib/ -lpcap -lm -lcurl -lz  -pthread -lmicrohttpd -lpolarssl
GCCFLAGS=-Wall -Werror -O2 -I$(PREFIX)/toolchain/include/ -I$(PREFIX)/target/usr/include/ 
LDFLAGS=-L$(PREFIX)/toolchain/lib/ -L$(PREFIX)/target/usr/lib/ -lpcap -lm -pthread -lpolarssl -lpthread -lmicrohttpd

TARGET=rssi-list

all: $(TARGET)

rssi-list: rssi_list.o main.o pcap-thread.o radiotap.o
	$(GCC) $(LDFLAGS) -o rssi-list rssi_list.o main.o pcap-thread.o radiotap.o

radiotap.o: radiotap.c radiotap.h radiotap_iter.h platform.h
	$(GCC) $(GCCFLAGS) -c -o radiotap.o radiotap.c

pcap-thread.o: pcap-thread.c 
	$(GCC) $(GCCFLAGS) -o pcap-thread.o -c pcap-thread.c 

rssi_list.o: rssi_list.c
	$(GCC) $(GCCFLAGS) -o rssi_list.o -c rssi_list.c 

# helloworld.o:hellobrowser.c
# 	$(GCC) $(LDFLAGS) -o hellobrowser.o -c hellobrowser.c 

main.o: main.c 
	$(GCC) $(GCCFLAGS) -o main.o -c main.c 


%.o: %.c %.h
	$(GCC) $(GCCFLAGS) -o $@ -c $<

#helloworld.o: helloworld.c
#	$(GCC) $(ALLFLAGS)  helloworld.c  -o helloworld.o

clean:
	rm -f *.o
	rm -f rssi-list

backupclean:
	rm -f *~
