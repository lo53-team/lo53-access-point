// #include "pcap-thread.h"
// #include "rssi_list.h"
// #include <pcap.h>
// #include "radiotap.h"
// #include "platform.h"
// #include "radiotap_iter.h"
// #include <semaphore.h>
// #include <signal.h>

// extern volatile sig_atomic_t got_sigint;
// extern Element * rssi_list;
// extern sem_t synchro;

// void *pcap_function(void *arg)
// {
//   char *iface = (char *) arg;
//   char errbuf[PCAP_ERRBUF_SIZE];
//   pcap_t * handle = NULL;
//   struct ieee80211_radiotap_header * rtap_head;
//   struct ieee80211_header * eh;
//   struct pcap_pkthdr header;
//   const u_char * packet;
//   u_char * mac;
//  // u_char first_flags;
//  // int offset = 0;
//   //char rssi;
//   Element * dev_info;
//   // radiotap iterator used to iterate over radiotap headers
//   struct ieee80211_radiotap_iterator* iterator = malloc(sizeof(struct ieee80211_radiotap_iterator));

//   // Open pcap handle to sniff traffic
//   handle = pcap_open_live(iface, BUFSIZ, 1, 1000, errbuf);
//   if (handle == NULL) {
//     printf("Could not open pcap on %s\n", iface);
//     pthread_exit((void *) -1);
//   }
  
//   while (got_sigint == 0) {
//     packet = pcap_next(handle, &header);
//     if (!packet)
//       continue;
//     rtap_head = (struct ieee80211_radiotap_header *) packet;
//     int16_t len = le16_to_cpu(rtap_head->it_len);
//    // int len = (int) rtap_head->it_len[0] + 256 * (int) rtap_head->it_len[1];
//     eh = (struct ieee80211_header *) (packet + len);
    
//     //eh = (struct ieee80211_header *) (packet + len);
//     if ((eh->frame_control & 0x03) == 0x01) {
//       mac = eh->source_addr;
//       // first_flags = rtap_head->it_present[0];
//       // offset = 8;
//       // offset += ((first_flags & 0x01) == 0x01) ? 8 : 0 ;
//       // offset += ((first_flags & 0x02) == 0x02) ? 1 : 0 ;
//       // offset += ((first_flags & 0x04) == 0x04) ? 1 : 0 ;
//       // offset += ((first_flags & 0x08) == 0x08) ? 4 : 0 ;
//       // offset += ((first_flags & 0x10) == 0x10) ? 2 : 0 ;
//       // rssi = *((char *) rtap_head + offset) - 0x100;
//       // printf("%d bytes -- %02X:%02X:%02X:%02X:%02X:%02X -- RSSI: %d dBm\n",
//       //        len, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], (int) rssi);

//       if(!ieee80211_radiotap_iterator_init(iterator, rtap_head, len, NULL) == 0) {
//         printf("Error! Iterator init\n");
//         continue;
//       }
//       sem_wait(&synchro);
//        // iterate through header fields until finding header IEEE80211_RADIOTAP_DBM_ANTSIGNAL
//       int err;
//       while (! (err = ieee80211_radiotap_iterator_next(iterator))) {
//         if(iterator->this_arg_index == IEEE80211_RADIOTAP_DBM_ANTSIGNAL) {
//           // value is in this_arg
//           int8_t rssi = (int8_t)*(iterator->this_arg);

//           printf("Adding value: %d to %02X:%02X:%02X:%02X:%02X:%02X  rssi : %d dBm\n",
//             rssi, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5],(int)rssi );
//       // We got some message issued by a terminal (FromDS=0,ToDS=1)
//           if ((dev_info = find_mac(rssi_list, mac)) == NULL) {
//            dev_info = add_element(&rssi_list, mac);
//          }
//          clear_outdated_values(&dev_info->measurements);
//          add_value(&dev_info->measurements, (int) rssi);
//        }
//      }
//      sem_post(&synchro);

//    }
//     }
//    pcap_close(handle);

//    pthread_exit((void *) 0);
//       free(iterator);
// }
#include "pcap-thread.h"
#include "rssi_list.h"
#include "radiotap.h"
#include "radiotap_iter.h"
#include "platform.h"
#include <pcap.h>
#include <semaphore.h>
#include <signal.h>


extern volatile sig_atomic_t got_sigint;
extern Element * rssi_list;
extern sem_t synchro;

void *pcap_function(void *arg)
{
  char *iface = (char *) arg; // interface
  char errbuf[PCAP_ERRBUF_SIZE]; // error buffer (unused)
  pcap_t * handle = NULL; // pcap handle
  struct ieee80211_radiotap_header * rtap_head; // radiotap header
  struct ieee80211_header * eh; // wifi header
  struct pcap_pkthdr header;  // pcap header (unused)
  const u_char * packet; // raw content of captured packet
  u_char * mac; // source macaddress


  // radiotap iterator used to iterate over radiotap headers
  struct ieee80211_radiotap_iterator* iterator = malloc(sizeof(struct ieee80211_radiotap_iterator));


  // Open pcap handle to sniff traffic
  handle = pcap_open_live(iface, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    printf("Could not open pcap on %s\n", iface);
    pthread_exit((void *) -1);
  }

  // Loop until interrupted
  while (got_sigint == 0) {
    packet = pcap_next(handle, &header);
    if (!packet)
      continue;

    rtap_head = (struct ieee80211_radiotap_header *) packet;

    int16_t len = le16_to_cpu(rtap_head->it_len);


    eh = (struct ieee80211_header *) (packet + len);

    // Check if FromDS=0, ToDS=1
    if ((eh->frame_control & 0x03) == 0x01) {
      mac = eh->source_addr;

      // if the mac is not being monitored, ignore packet
      Element * relevant = find_mac(rssi_list, mac);
      if(relevant == NULL) {
        printf("Adding value: to %02X:%02X:%02X:%02X:%02X:%02X\n",
             mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
        printf("patatipata");
        continue;
      }

      if(!ieee80211_radiotap_iterator_init(iterator, rtap_head, len, NULL) == 0) {
        printf("Error! Iterator init\n");
        continue;
      }

      sem_wait(&synchro);

      // clear outdated values
      clear_outdated_values(&(relevant->measurements));

      // iterate through header fields until finding header IEEE80211_RADIOTAP_DBM_ANTSIGNAL
      int err;
      while (! (err = ieee80211_radiotap_iterator_next(iterator))) {
        printf("je suis dans la boucle\n");
        if(iterator->this_arg_index == IEEE80211_RADIOTAP_DBM_ANTSIGNAL) {
          // value is in this_arg
          int8_t rssi = (int8_t)*(iterator->this_arg);

          printf("Adding value: %d to %02X:%02X:%02X:%02X:%02X:%02X\n",
            rssi, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

          // add the value to the measurements
          add_value(&(relevant->measurements), rssi);
          break;
        }
      }
      sem_post(&synchro);
    }
  }
  pcap_close(handle);
  pthread_exit((void *) 0);
  free(iterator);
}